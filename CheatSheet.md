**Writing Pin**-

nrf_gpio_pin_write(uint32_t pin_number, uint32_t value);      (Def-nrf_gpio.h)

nrf_gpio_pin_set(uint32_t pin_number);                        (Def-nrf_gpio.h)

nrf_gpio_pin_clear(uint32_t pin_number);                      (Def-nrf_gpio.h)

nrf_gpio_pin_toggle(uint32_t pin_number);                     (Def-nrf_gpio.h)

nrf_gpio_cfg_output(uint32_t pin_number);                     (Def-nrf_gpio.h)

nrf_gpio_cfg_input(uint32_t pin_number, nrf_gpio_pin_pull_t pull_config);    (Def-nrf_gpio.h)

nrf_gpio_cfg_default(uint32_t pin_number);                    (Def-nrf_gpio.h)

nrf_gpio_pin_read(uint32_t pin_number);                       (Def-nrf_gpio.h)[Returns 0 if low, +ve int if high]

nrf_gpio_pin_out_read(uint32_t pin_number);                   (Def-nrf_gpio.h)

**Delay**-

nrf_delay_ms(uint32_t number_of_ms);						  (Def-nrf_delay.h)

nrf_delay_us(uint32_t number_of_us);						  (Def-nrf_delay.h)

**UART**-

app_uart_get(uint8_t * p_byte);                               (Def-app_uart_fifo.c)[Read a byte from UART],[For usage, declare variable, pass the address as parameter in place of pointer]

app_uart_put(uint8_t byte);                                   (Def-app_uart_fifo.c)[put a byte on UART]

APP_UART_FIFO_INIT(&comm_params,
                         UART_RX_BUF_SIZE,
                         UART_TX_BUF_SIZE,
                         uart_error_handle,
                         APP_IRQ_PRIORITY_LOWEST,
                         err_code);

const app_uart_comm_params_t comm_params =
      {
          RX_PIN_NUMBER,
          TX_PIN_NUMBER,
          RTS_PIN_NUMBER,
          CTS_PIN_NUMBER,
          APP_UART_FLOW_CONTROL_DISABLED,
          false,
          UART_BAUDRATE_BAUDRATE_Baud115200
      };													  (Def-app_uart.h)
	  
APP_ERROR_CHECK(err_code);	  